class AdvertMailer < ActionMailer::Base
  default from: "maris@maris-cilitis.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.advert_mailer.adverts.subject
  #
  def adverts(to, adverts)
    @adverts = adverts
    mail to: to, subject: "jauni sludinajumi."
  end
end
