class Advert < ActiveRecord::Base
  attr_accessible :identifier, :link, :sent, :street, :room_count, :price
end
