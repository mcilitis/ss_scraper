# encoding=utf-8

class Scrapper
  attr_reader :url
  URLS = [
    "https://www.ss.lv/lv/real-estate/flats/riga/centre/",
  ]
  RECEIVERS = [
    "mar4a999@gmail.com",
  ]

  def scrape_site
    adverts = []

    URLS.each do |url|
      page = agent.get(url)

      page.search("form[id='filter_frm'] table[align='center'] tr").drop(1).each do |row|
        if Advert.find_by_identifier(row["id"])
          break
        else
          adverts << create_advert(row)
        end
      end
    end

    if adverts.any?
      send_email(adverts.compact)
    end
  end

  private

  def agent
    @agent ||= Mechanize.new
  end

  def create_advert(row)
    Advert.create(
      identifier: row["id"],
      link: "http://www.ss.lv/"+row.search("td div[class='d1'] a").first["href"],
      sent: false,
      street: row.search("td")[3].content,
      room_count: row.search("td")[4].content,
      price: row.search("td")[9].content
    )
  rescue
    nil
  end

  def send_email(adverts)
    RECEIVERS.each do |recipient|
      adverts = Advert.where(sent: false)
      AdvertMailer.adverts(recipient, adverts).deliver
      adverts.update_all(sent: true)
    end
  end
end
