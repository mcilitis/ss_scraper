require 'rufus-scheduler'

namespace :scraper do
  desc "scraping adverts"

  task :start => :environment do
    scheduler = Rufus::Scheduler.new

    scheduler.every 10 do
      Scrapper.new.scrape_site
    end
    scheduler.join
  end
end
