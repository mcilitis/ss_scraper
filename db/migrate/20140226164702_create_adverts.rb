class CreateAdverts < ActiveRecord::Migration
  def change
    create_table :adverts do |t|
      t.string :identifier
      t.string :link
      t.boolean :sent

      t.timestamps
    end
  end
end
