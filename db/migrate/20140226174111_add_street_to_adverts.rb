class AddStreetToAdverts < ActiveRecord::Migration
  def change
    add_column :adverts, :street, :string
  end
end
