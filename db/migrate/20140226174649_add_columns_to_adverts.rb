class AddColumnsToAdverts < ActiveRecord::Migration
  def change
    add_column :adverts, :room_count, :string
    add_column :adverts, :price, :string
  end
end
