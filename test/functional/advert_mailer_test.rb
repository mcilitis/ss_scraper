require 'test_helper'

class AdvertMailerTest < ActionMailer::TestCase
  test "adverts" do
    mail = AdvertMailer.adverts
    assert_equal "Adverts", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
